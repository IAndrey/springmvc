package ru.my.SpringCourse.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MySpringDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    protected Class<?>[] getRootConfigClasses() {
        return new Class[0];
    }

//  Указываем, что в качестве конфига для Spring, будет наш класс SpringConfig.
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] {SpringConfig.class};
    }
//  Указываем что все запросы от пользователя {"/"}, посылаются на DispatcherServlet.
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }
}
